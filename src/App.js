import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

var DATA = [
  {
    author: 'John Celery',
    bio: 'I am BIO read me.',
    photo: 'https://images.unsplash.com/photo-1464054313797-e27fb58e90a9?dpr=1&auto=format&crop=entropy&fit=crop&w=1500&h=996&q=80',
  },
  {
    author: 'Steve Sexblood',
    bio: 'BIO 2 heheh.',
    photo: 'https://images.unsplash.com/photo-1458668383970-8ddd3927deed?dpr=1&auto=format&crop=entropy&fit=crop&w=1500&h=1004&q=80',
  },
  {
    author: 'Ken Dolans',
    bio: 'ABCDE WHTF.',
    photo: 'https://images.unsplash.com/photo-1422393462206-207b0fbd8d6b?dpr=1&auto=format&crop=entropy&fit=crop&w=1500&h=1000&q=80',
  },
]

function Icon(props) {
  return (
    <i onClick={props.onClick}>
      <img className="picbox" src={props.value} />
    </i>
  )
}

class IconBox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      icons: Array(3).fill('https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-68-32.png'),
      isLiked: false
    }
  }

  handleClick(i) {
    const icons = this.state.icons.slice()
    icons[i] = this.state.isLiked ? 'https://cdn2.iconfinder.com/data/icons/instagram-ui/48/jee-68-32.png' : 'https://cdn1.iconfinder.com/data/icons/instagram-ui-colored/48/JD-07-32.png'
    this.setState({
      icons: icons,
      isLiked: !this.state.isLiked,
    })
  }

  renderIcon(i) {
    return (
      <Icon
        value={this.state.icons[i]}
        onClick={() => this.handleClick(i)}
      />
    )
  }
  render() {
    return (
      <div>
        {this.renderIcon(0)}
      </div>
    )
  }
}

function Background(props) {
  return (
    <img src={props.photo} />
  )
}

function Background(props) {
  return (
    <img src={props.photo} />
  )
}

function Author(props) {
  return (
    <h1>{props.author}</h1>
  )
}

function Bio(props) {
  return (
    <h2 class="animate-text">{props.bio}</h2>
  )
}

class SocialCard extends Component {
  render(){
    return (
      DATA.map((data) => {
        return (<div class="tile">
        <Background photo={data.photo} />
        <div class="text">
          <Author author={data.author} />
          <Bio bio={data.bio} />
          <div class="dots">
            <IconBox />
          </div>
        </div>
      </div> )
      })
    )
  }
}

class App extends Component {
  render() {
    return (
          <div class="wrap">
            <SocialCard />
          </div>
    )
  }
}



export default App;
